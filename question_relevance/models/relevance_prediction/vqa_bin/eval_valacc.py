"""
Computes normalized/unnormalized accuracy files for vqa-bin, vqa-bin-premise model predictions.
author: Aroma Mahendru (maroma@vt.edu)
"""
import sys
import json
import torchfile
import numpy as np

def compute_acc(tfile):
	tfile = torchfile.load(tfile)
	scores = tfile['scores']
	labels = tfile['labels']
	qids = tfile['qid']

	print("total: "+str(len(labels)))
	preds = np.argmax(scores, axis=1)
	preds += 1
	acc = float((preds == labels).sum())/float(len(labels))

	zero_count = 0
	zero_correct = 0
	one_count = 0
	one_correct = 0
	for i in range(len(labels)):
		if labels[i] == 1:
			zero_count = zero_count + 1
			if preds[i] == 1:
				zero_correct = zero_correct + 1
		else:
			one_count = one_count + 1
			if preds[i] == 2:
				one_correct = one_correct + 1
	zero_acc = float(zero_correct)/float(zero_count)
	one_acc = float(one_correct)/float(one_count)
	nacc = zero_acc*float(zero_count)/float(len(labels))
	nacc = nacc + one_acc*float(one_count)/float(len(labels))
	return acc, nacc

def main():
	args = sys.argv[1:]
	acc, nacc = compute_acc(args[0])
	print('val acc : '+str(acc))
	print('norm acc: '+str(nacc))

if __name__ == '__main__':
	main()
