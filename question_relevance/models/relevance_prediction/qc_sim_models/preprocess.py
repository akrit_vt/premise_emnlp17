import json
import operator
import random
import collections

def build_coco_imgpath(phase, imid):
	return "COCO_" + phase + "2014_" + str(imid).zfill(12) + ".jpg"

def build_mapping(data):
	mapping = {}
	for ix, val in enumerate(data):
		mapping[val["question_id"]] = ix
	return mapping

def parse(data, split, tups):
	final = dict()

	mapping = build_mapping(tups)

	for ix, val in enumerate(data):

		tuplist = tups[mapping[val["qid"]]]["tuples"]
		for ix2, val2 in enumerate(val["tuplist"]):
			key1 = str(val["qid"]) + "_" + str(val2["rel_imid"]) + "__relevant"
			final[key1] = {
				"image": build_coco_imgpath(split, val2["rel_imid"]),
				"label": 1,
				"lcounts": [1, 1, 1],
				"question": val["q"],
				"imtype": "coco" + split, 
				"tuplist": tuplist
			}
			is_attr = True if "_" in val2["rel_tuple"] else False

			if is_attr:
				imgpath = str(val2["irr_imid"]) + ".jpg"
				imtype = "visgen"
			else:
				imgpath = build_coco_imgpath(split, val2["irr_imid"])
				imtype = "coco" + split
			
			key2 = str(val["qid"]) + "_" + str(val2["irr_imid"]) + "__" + str(ix2) + "__irrelevant"
			final[key2] = {
				"image": imgpath,
				"label": 2,
				"lcounts": [2, 2, 2],
				"question": val["q"],
				"imtype": imtype,
				"tuplist": tuplist
			}
	return final

with open("../../../data/train/train.json") as f1, \
	 open("../../../data/test/test.json") as f2, \
	 open("store/vqa_oe_train_tuples.json") as f3, \
	 open("store/vqa_oe_val_tuples.json") as f4, \
	 open("../../../data/test/objects_test.json") as f5, \
	 open("../../../data/test/attributes_test.json") as f6:
 	train = json.load(f1)
	test = json.load(f2)
	
	otest = json.load(f5)
	atest = json.load(f6)
	
	traintups = json.load(f3)
	testtups = json.load(f4)
	
	qrpetrain = parse(train, "train", traintups)
	qrpetest = parse(test, "val", testtups)
	qrpetest_attr = parse(atest, "val", testtups)
	qrpetest_obj = parse(otest, "val", testtups)

	with open("input/qrpe_train.json", "w") as f1, open("input/qrpe_test.json", "w") as f2, \
		 open("input/qrpe_test_attr.json", "w") as f3, open("input/qrpe_test_obj.json", "w") as f4:
		json.dump(qrpetrain, f1, indent=4)
		json.dump(qrpetest, f2, indent=4)
		json.dump(qrpetest_attr, f3, indent=4)
		json.dump(qrpetest_obj, f4, indent=4)