# QC-Sim models

Code for question-caption-premise similarity style models described in: 
  
The Promise of Premise: Harnessing Question Premises in Visual Question Answering (EMNLP 2017) ��https://arxiv.org/abs/1705.00601

### Requirements:

- Keras: ```pip install keras==2.0.2```
- Scikit-learn: ```pip install -U scikit-learn```
- Gensim: ```pip install -U gensim```

### Preprocessing:

The expected format of the QRPE dataset is already included under input/. If you would like to use that, proceed to run instructions.

If you would like to regenerate input data, run:

```python preprocess.py```

### Run instructions:
```python run_model.py --model_type [option1] --w2v_path [option2] --loadWeights [option3]```

Options:

* [option1] :
    * qc: for question-caption similarity
    * pc: for premise-caption similarity
    * qpc: for question-premise-caption similarity
  
* [option2] : Path to pretrained w2v vector corpus � GoogleNews-vectors-negative300.bin. Can be downloaded from https://code.google.com/archive/p/word2vec/.
  
* [option3] : (optional) Path to pretrained model checkpoints. 

Please contact virajp@gatech.edu if you have questions.
