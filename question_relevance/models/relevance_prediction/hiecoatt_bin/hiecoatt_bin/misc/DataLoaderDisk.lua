require 'hdf5'
cjson = require 'cjson'
local utils = require 'misc.utils'
local DataLoader = torch.class('DataLoader')

function DataLoader:__init(opt)
    
    self.split = 0
    local file = io.open(opt.input_json, 'r')
    local text = file:read()
    file:close()
    self.json_file = cjson.decode(text)

    local count = 0
    for i, w in pairs(self.json_file['ix_to_word']) do count = count + 1 end
    self.vocab_size = count

    local file = io.open(opt.input_json_coco, 'r')
    local text = file:read()
    file:close()
    self.coco_train_dict = cjson.decode(text)

    local file = io.open(opt.input_json_coco_val, 'r')
    local text = file:read()
    file:close()
    self.coco_val_dict = cjson.decode(text)

    local file = io.open(opt.input_json_vg, 'r')
    local text = file:read()
    file:close()
    self.vg_dict = cjson.decode(text)

    self.fv_coco_train = torch.load(opt.input_img_coco_train)
    self.fv_coco_val = torch.load(opt.input_img_coco_val)
    self.fv_vg = torch.load(opt.input_img_vg)

    print('DataLoader loading h5 question file: ', opt.input_ques_h5)
    local h5_file = hdf5.open(opt.input_ques_h5, 'r')
    self.ques_train = h5_file:read('/ques_train'):all()
    self.ques_len_train = h5_file:read('/ques_length_train'):all()
    self.answer = h5_file:read('/labels_train'):all()
    self.ttype = h5_file:read('/ttype_train'):all()

    self.ques_test = h5_file:read('/ques_test'):all()
    self.ques_len_test = h5_file:read('/ques_length_test'):all()
    self.ans_test = h5_file:read('/labels_test'):all()
    self.ttype_test = h5_file:read('/ttype_test'):all()

    h5_file:close()
    print('Transform the image feature...')

    --if opt.h5_img_file_train ~= nil then
    if opt.feature_type == 'VGG' then
        self.fv_coco_train = self.fv_coco_train:view(-1, 196, 512):contiguous()
        self.fv_coco_val = self.fv_coco_val:view(-1, 196, 512):contiguous()
        self.fv_vg = self.fv_vg:view(-1, 196, 512):contiguous()
    elseif opt.feature_type == 'Residual' then
    --    self.fv_im_train = self.fv_im_train:view(-1, 196, 2048):contiguous()
        self.fv_coco_train = self.fv_coco_train:view(-1, 196, 2048):contiguous()
        self.fv_coco_val = self.fv_coco_val:view(-1, 196, 2048):contiguous()
        self.fv_vg = self.fv_vg:view(-1, 196, 2048):contiguous()
    else
        error('feature type error')
    end
    --end

    --print('DataLoader loading json file: ', opt.json_file)
    --local json_file = utils.read_json(opt.json_file)
    self.ix_to_word = self.json_file['ix_to_word']
    --self.ix_to_ans = json_file['ix_to_ans']

    self.seq_length = self.ques_train:size(2)

    collectgarbage() -- do it often and there is no harm ;)
end

function DataLoader:resetIterator(split)
  self.split = split
end

function DataLoader:getVocabSize()
    return self.vocab_size
end

function DataLoader:getSeqLength()
  return self.seq_length
end

function DataLoader:getDataNum(split)
    if split == 0 or split == 1 then
        return self.answer:size(1)
    else
        return self.ans_test:size(1)
    end
end

function DataLoader:getBatch(opt)
    local split = utils.getopt(opt, 'split') -- lets require that user passes this in, for safety
    local batch_size = utils.getopt(opt, 'batch_size', 128)

    local data = {}
    -- fetch the question and image features.
    if split == 0 or split == 1 then

        local nqs= self.ques_train:size(1)
        local qinds=torch.LongTensor(batch_size):fill(0) 
        local iminds=torch.LongTensor(batch_size):fill(0)
        data.images = torch.LongTensor(batch_size, self.fv_coco_train:size()[2], self.fv_coco_train:size()[3]):fill(0)
        -- we use the last val_num data for validation (the data already randomlized when created)

        for i=1,batch_size do
            qinds[i]=torch.random(nqs) 
            if self.ttype[qinds[i]] == 1 then
                data.images[i] = self.fv_coco_train[self.coco_train_dict[self.json_file['imid_train'][qinds[i]]]+1]
            else
                data.images[i] = self.fv_vg[self.vg_dict[self.json_file['imid_train'][qinds[i]]]+1]
            end
        end

        data.questions = self.ques_train:index(1,qinds)
        data.ques_len = self.ques_len_train:index(1, qinds)
        data.answer = self.answer:index(1,qinds) 
    
    
    else
        local nqs= self.ques_test:size(1)
        local qinds=torch.LongTensor(batch_size):fill(0) 
        local iminds=torch.LongTensor(batch_size):fill(0)   
    
        data.images = torch.LongTensor(batch_size, self.fv_coco_val:size()[2],self.fv_coco_val:size()[3]):fill(0)
        -- we use the last val_num data for validation (the data already randomlized when created)

        for i=1,batch_size do
            qinds[i]=torch.random(nqs) 
            if self.ttype_test[qinds[i]] == 1 then
                data.images[i] = self.fv_coco_val[self.coco_val_dict[self.json_file['imid_test'][qinds[i]]]+1]
            else
                data.images[i] = self.fv_vg[self.vg_dict[self.json_file['imid_test'][qinds[i]]]+1]
            end
        end

        data.questions = self.ques_test:index(1,qinds)
        data.ques_len = self.ques_len_test:index(1, qinds)
        data.answer = self.ans_test:index(1,qinds)
    end
    return data
end